package com.product.apifunctions.repositories;

import java.util.Optional;

import org.springframework.data.mongodb.core.aggregation.VariableOperators.Map;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.product.apifunctions.models.Product;



@Repository
public interface IProductRepository extends MongoRepository<Product, String> {
	
	
	
	
}

package com.product.apifunctions.models;



import java.time.LocalDate;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.format.annotation.DateTimeFormat;

import com.mongodb.annotations.NotThreadSafe;
import com.mongodb.lang.NonNull;

@Document(collection = "products")
public class Product {
	
	@Id
	public String id;
	
	@NonNull
	private String name;
	@NonNull
	private Double price_real;
	@NonNull
	private Double price_dollar;
	
	private LocalDate creation_date;

	

	public Product(String name, Double price_real, Double price_dollar) {
		super();
		this.name = name;
		this.price_real = price_real;
		this.price_dollar = price_dollar;
	}

	public String get_id() {
		return id;
	}

	public void set_id(String _id) {
		this.id = _id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getPrice_real() {
		return price_real;
	}

	public void setPrice_real(Double price_real) {
		this.price_real = price_real;
	}

	public Double getPrice_dollar() {
		return price_dollar;
	}

	public void setPrice_dollar(Double price_dollar) {
		this.price_dollar = price_dollar;
	}

	public LocalDate getCreation_date() {
		return creation_date;
	}

	public void setCreation_date(LocalDate creation_date) {
		this.creation_date = creation_date;
	}

	

	
	
}

package com.api;


import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.product.apifunctions.models.Product;
import com.product.apifunctions.repositories.IProductRepository;


@SpringBootTest
class ApiTests {

	@Test
	void test() {
		fail("Not yet implemented");
	}
	
	
	@Autowired
	IProductRepository repository;



	Product produto1, produto2, produto3;

	@BeforeEach
	public void setUp() {

		repository.deleteAll();

		produto1 = repository.save(new  Product("caderneta", 40.00, 7.50));
		produto2 = repository.save(new Product("pacote cambial", 500.00, 2500.00));
		produto3 = repository.save(new Product("game monopoly", 100.00, 20.00));
	}

	@Test
	public void setsIdOnSave() {

		Product produto1 = repository.save(new Product("caderno", 4.50, 1.0 ));

		assertThat(produto1.id).isNotNull();
	}

	

}

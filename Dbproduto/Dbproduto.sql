/*
 Navicat Premium Data Transfer

 Source Server         : MongoConnection
 Source Server Type    : MongoDB
 Source Server Version : 40208
 Source Host           : localhost:27017
 Source Schema         : Dbproduto

 Target Server Type    : MongoDB
 Target Server Version : 40208
 File Encoding         : 65001

 Date: 24/06/2020 23:05:22
*/


// ----------------------------
// Collection structure for products
// ----------------------------
db.getCollection("products").drop();
db.createCollection("products");

// ----------------------------
// Documents of products
// ----------------------------
db.getCollection("products").insert([ {
    _id: ObjectId("5ef3f5516e870d79fe8a37e7"),
    name: "teste",
    "price_real": 40,
    "price_dollar": 10,
    "creation_date": ISODate("2020-02-02T03:00:00.000Z"),
    _class: "com.product.apifunctions.models.Product"
} ]);
db.getCollection("products").insert([ {
    _id: ObjectId("5ef3f5976e870d79fe8a37e8"),
    name: "teste..",
    "price_real": 4000,
    "price_dollar": 1000,
    "creation_date": ISODate("2020-02-02T03:00:00.000Z"),
    _class: "com.product.apifunctions.models.Product"
} ]);
db.getCollection("products").insert([ {
    _id: ObjectId("5ef3f5b16e870d79fe8a37e9"),
    name: "produto",
    "price_real": 1,
    "price_dollar": 4.5,
    "creation_date": ISODate("2020-02-02T03:00:00.000Z"),
    _class: "com.product.apifunctions.models.Product"
} ]);
db.getCollection("products").insert([ {
    _id: ObjectId("5ef3f5e66e870d79fe8a37ea"),
    name: "produto",
    "price_real": 4.5,
    "price_dollar": 1,
    "creation_date": ISODate("2020-02-02T03:00:00.000Z"),
    _class: "com.product.apifunctions.models.Product"
} ]);
